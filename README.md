Git Merging Tutorial
========

_By Rebecca Rashkin, January 2020_

This tutorial addresses the coordination of development efforts between two or more entities developing on a single branch. For information about merging two branches together, check out <https://www.atlassian.com/git/tutorials/using-branches/git-merge>.

Prerequisites
--------

It is assumed that the reader has basic understanding of UNIX command line operation and Git commands.

For an introduction to basic Git functionality, watch the videos from the Git site: <https://git-scm.com/videos>

Note
--------
This tutorial is written in markdown (<http://markdownguide.org/getting_started>), so it is best viewed in on the Git client web interface, or another viewer in order for formatting to be displayed properly.

---

This tutorial will go through several different scenarios in which the same repository is cloned separate locations. The scenarios are outlined as follows:

1. Different lines of the same file are modified from two instances of the cloned repository.
2. The same line(s) from the same file is modified from two instances of the cloned repository.
3. Different files are modified from two instances of the cloned repository.

To simulate each of these scenarios, you must clone the same repository on two separate workstations or in two different locations on the same workstation. 

In this example, we will refer to the first instance of the repository as `clone1` and the second instance of the repository as `clone2`.

__This is Important!__

If going through this tutorial using one workstation, be sure not to clone the repository within itself.

__Another Note__

The author is using a repository named flux.git.

---

Scenario 1
--------
_Different lines of the same file are modified from two separate instances of the same cloned repository._

To simulate this scenario, perform the following steps:

1. In `clone1`, add and push a file named `samefile.txt`:

    - Create empty file.

        _(from clone1)_
        `touch samefile.txt`

    - Modify `samefile.txt` to contain the following text.

        ```
        original line 1
        original line 2
        original line 3
        ```

    - Stage the file to be committed.

        _(from clone1)_
        `git add samefile.txt`

    - Commit the file with a comment.

        _(from clone1)_
        `git commit -m 'initial add of samefile.txt to repo'`

        _Note: Output from the author's command line will be included. Exact output may differ from your system._

        ```
        [master 40456f5] initial add of samefile.txt to repo
        1 file changed, 3 insertions(+)
        create mode 100644 samefile.txt
        ```

    - Push the file to the repository.

        _(from clone1)_
        `git push`

        ```
        Enumerating objects: 4, done.
        Counting objects: 100% (4/4), done.
        Delta compression using up to 24 threads
        Compressing objects: 100% (2/2), done.
        Writing objects: 100% (3/3), 319 bytes | 53.00 KiB/s, done.
        Total 3 (delta 0), reused 0 (delta 0)
        To bitbucket.org:rebeccajennifer/flux.git
           b74d604..40456f5  master -> master
        ```


2. From `clone2`, pull the repository to get the latest version, modify the file, and push the changes:

    - In `clone2` (the second instance of the same repository), pull to get the latest version. 

        _(from clone2)_
        `git pull`
      
        ```
        remote: Counting objects: 3, done.
        remote: Compressing objects: 100% (2/2), done.
        remote: Total 3 (delta 0), reused 0 (delta 0)
        Unpacking objects: 100% (3/3), done.
        From bitbucket.org:rebeccajennifer/flux
           b74d604..40456f5  master     -> origin/master
        Updating b74d604..40456f5
        Fast-forward
         samefile.txt | 3 +++
         1 file changed, 3 insertions(+)
         create mode 100644 samefile.txt
        ```

    - Modify `samefile.txt` to contain the following text:

        ```
        original line 1
        new line added from clone2
        original line 2
        new line added from clone2
        original line 3
        ```

    - Save the file, then add, commit, and push the changes.

        _(from clone2)_
        `git add samefile.txt`

        `git commit -m 'modified samefile.txt for merging tutorial'`

        ```
        [master 7a9a6ec] modified samefile.txt for merging tutorial
         1 file changed, 2 insertions(+)
        ```

        _(from clone2)_
        `git push`

        ```
        Enumerating objects: 5, done.
        Counting objects: 100% (5/5), done.
        Delta compression using up to 24 threads
        Compressing objects: 100% (3/3), done.
        Writing objects: 100% (3/3), 345 bytes | 31.00 KiB/s, done.
        Total 3 (delta 0), reused 0 (delta 0)
        To bitbucket.org:rebeccajennifer/flux.git
           40456f5..7a9a6ec  master -> master
        ```

3. From `clone1` make modifications without pulling first.
  
    _Note: In this tutorial we are intentionally not performing a pull before modifying the file for the purpose of demonstration. However, in practice you should always pull before modifying a file to reduce the change of having a merge conflict._

    - Modify `samefile.txt` to contain the following text:

        ```
        new line added from clone1!!!
        original line 1
        original line 2
        original line 3
        new line added from clone1!!!
        ```

    - Save the file, then add, commit, and push the changes.

        _(from clone1)_
        `git add samefile.txt`

        `git commit -m 'modified samefile.txt from clone1 without pulling first for merging tutorial'`

        ```
        [master 995f093] modified samefile.txt from clone1 without pulling first for merging tutorial
         1 file changed, 5 insertions(+)
        ```
      
        _(from clone1)_
        `git push`

        ```
        To bitbucket.org:rebeccajennifer/flux.git
         ! [rejected]        master -> master (fetch first)
        error: failed to push some refs to 'git@bitbucket.org:rebeccajennifer/flux.git'
        hint: Updates were rejected because the remote contains work that you do
        hint: not have locally. This is usually caused by another repository pushing
        hint: to the same ref. You may want to first integrate the remote changes
        hint: (e.g., 'git pull ...') before pushing again.
        hint: See the 'Note about fast-forwards' in 'git push --help' for details.
        ```
        Git rejected the push because there were changes made to the master that were not incorporated into the local version.

    - To address this issue, pull the latest version from master.

        _(from clone1)_
        `git pull`

        At this point, the default text editor will open for a comment. Oftentimes the text editor is Vim, which is not initially intuitive to use.

        ```
        Merge branch 'master' of bitbucket.org:rebeccajennifer/flux

        # Please enter a commit message to explain why this merge is necessary,
        # especially if it merges an updated upstream into a topic branch.
        #
        # Lines starting with '#' will be ignored, and an empty message aborts
        # the commit.
        ```

        In Vim, to edit text you must switch between command mode and text editing mode. To insert text, use the command `i`. To go back to command mode, use the `Esc` key. To save and exit, from command mode type `:wq` (`w` for _write_ and `q` for _quit_).

        For a tutorial on vim, from the command line, type `vimtutor`, and for more information about Vim, visit <http://vim.org>.

    - After saving the merge comment, the command line interface will display:

        ```
        Auto-merging samefile.txt
        Merge made by the 'recursive' strategy.
        samefile.txt | 2 ++
        1 file changed, 2 insertions(+)
        ```

    - Now, if you view `samefile.txt` you will see that the modifications that were made from both `clone1` and `clone2` have been incorporated into samefile.txt.

        ```
        new line added from clone1!!!
        original line 1
        new line added from clone2
        original line 2
        new line added from clone2
        original line 3
        new line added from clone1!!!
        ```

Scenario 2
--------
_The same line(s) from the same file is modified from two instances of the cloned repository._

To simulate this scenario, perform the following steps:

1. In `clone1`, add and push a file named `merge_example.txt` containing the following text:

    ```
    original line 1
    original line 2
    original line 3
    ```

    _(from clone1)_
    `git add merge_example.txt; git commit -m 'initial add of merge_example.txt'; git push`

   
    ```
    [master aa9a6f8] initial add of merge_example.txt
     1 file changed, 3 insertions(+)
     create mode 100644 merge_example.txt
    Enumerating objects: 4, done.
    Counting objects: 100% (4/4), done.
    Delta compression using up to 24 threads
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (3/3), 314 bytes | 31.00 KiB/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    To bitbucket.org:rebeccajennifer/flux.git
       8d963de..aa9a6f8  master -> master
    ```

2. In `clone2` pull to get the latest version of the repository.

    _(from clone2)_
    `git pull`

    ```
    remote: Counting objects: 3, done.
    remote: Compressing objects: 100% (2/2), done.
    remote: Total 3 (delta 1), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.
    From bitbucket.org:rebeccajennifer/flux
       8d963de..aa9a6f8  master     -> origin/master
    Updating 8d963de..aa9a6f8
    Fast-forward
     merge_example.txt | 3 +++
     1 file changed, 3 insertions(+)
     create mode 100644 merge_example.txt
    ```

3. In `clone2`, modify `merge_example.txt` to the following text, and push the changes to the repository:
   
    ```
    original line 1
    changed line 2 from clone2
    original line 3
    ```

    _(from clone2)_
    `git add merge_example.txt; git commit -m 'modified merge_example.txt from clone2'; git push`

    ```
    [master 726c6a9] modified merge_example.txt from clone2
     1 file changed, 1 insertion(+), 1 deletion(-)
    Enumerating objects: 5, done.
    Counting objects: 100% (5/5), done.
    Delta compression using up to 24 threads
    Compressing objects: 100% (3/3), done.
    Writing objects: 100% (3/3), 314 bytes | 31.00 KiB/s, done.
    Total 3 (delta 1), reused 0 (delta 0)
    To bitbucket.org:rebeccajennifer/flux.git
       aa9a6f8..726c6a9  master -> master
    ```

4. In `clone1`, modify `merge_example.txt` to the following text, and push the changes to the repository:

    ```
    original line 1
    clone1 modification of line 2
    original line 2
    ```

    _(from clone1)_
    `git add merge_example.txt; git commit -m 'modification of merge_example.txt from clone1'; git push`

    ```
    [master 38e939f] modification of merge_example.txt from clone1
     1 file changed, 1 insertion(+), 1 deletion(-)
    To bitbucket.org:rebeccajennifer/flux.git
     ! [rejected]        master -> master (fetch first)
    error: failed to push some refs to 'git@bitbucket.org:rebeccajennifer/flux.git'
    hint: Updates were rejected because the remote contains work that you do
    hint: not have locally. This is usually caused by another repository pushing
    hint: to the same ref. You may want to first integrate the remote changes
    hint: (e.g., 'git pull ...') before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.
    ```

    Git rejected the push because there were changes made to the master that were not incorporated into the local version.
   
5. To address this issue, pull the latest version from the master, and merge the changes.

    _(from clone1)_
    `git pull`

    ```
    remote: Counting objects: 3, done.
    remote: Compressing objects: 100% (3/3), done.
    remote: Total 3 (delta 1), reused 0 (delta 0)
    Unpacking objects: 100% (3/3), done.
    From bitbucket.org:rebeccajennifer/flux
    aa9a6f8..726c6a9  master     -> origin/master
    Auto-merging merge_example.txt
    CONFLICT (content): Merge conflict in merge_example.txt
    Automatic merge failed; fix conflicts and then commit the result.
    ```

    At this point, the file `merge_example.txt` contains the changes made from `clone1` and `clone2` so the user must go in and manually update the file, looking at both versions and judging which lines to keep.
   
    The file `merge_example.txt` currently looks like this:
   
    ```
    original line 1
    <<<<<<< HEAD
    clone1 modification of line 2
    =======
    changed line 2 from clone2
    >>>>>>> 726c6a9f68b798773c2695a4d5318295bdd33433
    original line 3
    ```

6. Modify `merge_example.txt` to reflect the correct text, then push the latest version to the master.

    ```
    original line 1
    both changes were silly; lets keep this line instead
    original line 3
    ```
    _(from clone1)_
    `git add merge_example.txt; git commit -m 'merged versions of merge_example.txt'; git push`

    ```
    [master f85c875] merged versions of merge_example.txt
    Enumerating objects: 10, done.
    Counting objects: 100% (10/10), done.
    Delta compression using up to 24 threads
    Compressing objects: 100% (6/6), done.
    Writing objects: 100% (6/6), 661 bytes | 33.00 KiB/s, done.
    Total 6 (delta 2), reused 0 (delta 0)
    To bitbucket.org:rebeccajennifer/flux.git
       726c6a9..f85c875  master -> master
    ```

    The correct version is now in the repository.

7. Now go to `clone2` to update and make sure it has the latest version.

    _(from clone2)_
    `git pull`

    ```
    remote: Counting objects: 6, done.
    remote: Compressing objects: 100% (6/6), done.
    remote: Total 6 (delta 2), reused 0 (delta 0)
    Unpacking objects: 100% (6/6), done.
    From bitbucket.org:rebeccajennifer/flux
       726c6a9..f85c875  master     -> origin/master
    Updating 726c6a9..f85c875
    Fast-forward
     merge_example.txt | 2 +-
     1 file changed, 1 insertion(+), 1 deletion(-)
    ```

Scenario 3
--------
_Different files are modified from two instances of the cloned repository_

To simulate this scenario, perform the following steps:

1. In `clone1`, add and push files named `bunnies.txt` and `dogs.txt`:

    - Create empty files named `bunnies.txt` and `dogs.txt`.

        _(from clone1)_
        `touch bunnies.txt dogs.txt`

    - Modify files to contain the following text.

         _bunnies.txt_:

         ```
         flux
         bella
         ```

         _dogs.txt_:

         ```
         baxter
         chewy
         ```

    - Stage the files to be committed.

        _(from clone1)_
        `git add bunnies.txt dogs.txt`

    - Commit the files with a comment.

        _(from clone1)_
        `git commit -m 'initial add of bunnies.txt and dogs.txt'`


        ```	
        [master 83a44f9] initial add of bunnies.txt and dogs.txt
         2 files changed, 4 insertions(+)
         create mode 100644 bunnies.txt
         create mode 100644 dogs.txt
        ```	
    
    - Push the files to the repository.

        _(from clone1)_
        `git push`

        ```
        Enumerating objects: 5, done.
        Counting objects: 100% (5/5), done.
        Delta compression using up to 4 threads
        Compressing objects: 100% (2/2), done.
        Writing objects: 100% (4/4), 388 bytes | 29.00 KiB/s, done.
        Total 4 (delta 0), reused 0 (delta 0)
        To bitbucket.org:rebeccajennifer/flux.git
           9f73a20..83a44f9  master -> master
        ```

2. In `clone2` pull the latest version, modify `bunnies.txt` and push the changes.

    - Pull the latest version of the repository.

        _(from clone2)_
        `git pull`

    - Modify `bunnies.txt` to the following.

        ```
        flux
        bella
        snowflake
        ```

    - Push the modification to the repository.

        _(from clone2)_
        `git add bunnies.txt; git commit -m 'added snowflake to bunnies.txt'; git push`

        ```
        [master d36686a] added snowflake to bunnies.txt
         1 file changed, 1 insertion(+)
        Enumerating objects: 5, done.
        Counting objects: 100% (5/5), done.
        Delta compression using up to 4 threads
        Compressing objects: 100% (2/2), done.
        Writing objects: 100% (3/3), 291 bytes | 41.00 KiB/s, done.
        Total 3 (delta 1), reused 0 (delta 0)
        To bitbucket.org:rebeccajennifer/flux.git
           9b582b3..d36686a  master -> master
        ```

3. In `clone1` modify `dogs.txt` and push the changes.

    - Modify `dogs.txt` to the following.

        ```
        baxter
        chewbacca
        ```

    - Push the modification to the repository.

        _(from clone1)_
        `git add dogs.txt; git commit -m 'changed chewy to chewbacca in dogs.txt'; git push`

        ```
        [master 6c7fed1] changed chewy to chewbacca in dogs.txt
         1 file changed, 1 insertion(+), 1 deletion(-)
        To bitbucket.org:rebeccajennifer/flux.git
         ! [rejected]        master -> master (fetch first)
        error: failed to push some refs to 'git@bitbucket.org:rebeccajennifer/flux.git'
        hint: Updates were rejected because the remote contains work that you do
        hint: not have locally. This is usually caused by another repository pushing
        hint: to the same ref. You may want to first integrate the remote changes
        hint: (e.g., 'git pull ...') before pushing again.
        hint: See the 'Note about fast-forwards' in 'git push --help' for details.
        ```

        Git rejected the push because there were changes made to the master that were not incorporated into the local version.

4. To address this issue, pull the latest version from the master, and merge the changes.

    - Pull the latest version.

        _(from clone1)_
        `git pull`

        Once again, the default text editor will open for a comment. Add any additional comments, save the file and exit out of the text editor.

        ```
        Merge branch 'master' of bitbucket.org:rebeccajennifer/flux

        # Please enter a commit message to explain why this merge is necessary,
        # especially if it merges an updated upstream into a topic branch.
   
        # Lines starting with '#' will be ignored, and an empty message aborts
        # the commit.
        ```

        _Console output_

        ```
        remote: Counting objects: 6, done.
        remote: Compressing objects: 100% (4/4), done.
        remote: Total 6 (delta 2), reused 0 (delta 0)
        Unpacking objects: 100% (6/6), done.
        From bitbucket.org:rebeccajennifer/flux
        83a44f9..d36686a  master     -> origin/master
        Merge made by the 'recursive' strategy.
        bunnies.txt | 2 ++
        1 file changed, 2 insertions(+)
        ```

    - Now that the changes have been merged, you can push this version to the repository.

        _(from clone1)_
        `git push`

        ```
        Enumerating objects: 9, done.
        Counting objects: 100% (8/8), done.
        Delta compression using up to 4 threads
        Compressing objects: 100% (4/4), done.
        Writing objects: 100% (5/5), 545 bytes | 30.00 KiB/s, done.
        Total 5 (delta 2), reused 0 (delta 0)
        To bitbucket.org:rebeccajennifer/flux.git
           d36686a..a7c7219  master -> master
        ```

    - At this point, both `bunnies.txt` and `dogs.txt` will be updated to the latest version. This can be verified using `git status`

        ```
        $ git status
        On branch master
        Your branch is up to date with 'origin/master'.
        nothing to commit, working tree clean
        ```
